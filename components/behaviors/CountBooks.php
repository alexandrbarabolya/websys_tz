<?php
namespace app\components\behaviors;

class CountBooks extends \yii\base\Behavior
{
	public function events()
	{
		return [
			\yii\db\ActiveRecord::EVENT_AFTER_INSERT => 'countBooks',
			\yii\db\ActiveRecord::EVENT_AFTER_DELETE => 'countBooks',
			\yii\db\ActiveRecord::EVENT_BEFORE_DELETE => 'countBooks',
			\yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'countBooks',
		];
	}

	public function countBooks($event)
	{
		$model = \app\models\Author::findOne($this->owner->author_id);

		if($model)
		{
			$model->count_book = $model->getBooks()->count();
			$model->save();
		}
	}
}