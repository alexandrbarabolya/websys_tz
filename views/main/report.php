<?php
use yii\grid\GridView;
?>
	<h1>Отчет</h1>
	<h3>Загальна кількість книг в базі: <?= $bookCount; ?></h3>
	<h3>Загальна кількість авторів в базі: <?= $authorCount; ?></h3><br>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			//'id',
			'name',
			'year',
			[
				'label' => 'Count books',
				'value' => function($model)
					{
						return count($model->books);
					}
			],
		],
	]); ?>