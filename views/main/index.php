<?php
use yii\bootstrap\Nav;
/* @var $this yii\web\View */
?>

<?= Nav::widget([
    'options' => ['class' => 'nav nav-pills nav-stacked'],
    'items' => [
        ['label' => Yii::t('app', 'Catalog'), 'url' => ['/main/catalog']],
        ['label' => Yii::t('app', 'Report'), 'url' => ['/main/report']],
        ]
])
?>