<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Author;
use app\models\Book;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'description',
            'isbn',
            [
                'attribute' => 'author_id',
                'filter' => Author::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'author.name'
            ],
            'author.year',

        ],
    ]); ?>
</div>