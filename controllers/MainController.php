<?php

namespace app\controllers;

use app\models\Book;
use app\models\Author;
use yii\data\ActiveDataProvider;
use app\models\Search\BookSearch;
use app\models\Search\AuthorSearch;

class MainController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCatalog()
    {
    	$searchModel = new BookSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 15];

        return $this->render('catalog', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport()
    {
    	$bookCount = Book::find()->count();
    	$authorCount = Author::find()->count();

    	$searchModel = new AuthorSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('report', [
        	'bookCount' => $bookCount,
        	'authorCount' => $authorCount,
        	'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

}
