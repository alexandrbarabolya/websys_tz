<?php

use yii\db\Migration;

class m160719_175633_add_column_coun_book extends Migration
{
    public function up()
    {
        $this->addColumn('{{%author}}','count_book', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('{{%author}}','count_book', $this->integer(11));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
