<?php

use yii\db\Migration;

class m160719_154138_add_data extends Migration
{
    public function up()
    {
        $this->batchInsert('author', ['name','year'], [
            ['Федор Михайлович Достоевский','1821'],
            ['Тара́с Григо́рьевич Шевче́нко','1814'],
            ['Ива́н Я́ковлевич Франко́ ','1856'],
            ['Эрнест Миллер Хемингуэй','1948'],
            ['Джордж Го́рдон Ба́йрон','1925'],
        ]);

        $this->batchInsert('book', ['name','description','isbn','author_id'],  [
            ['Преступление и наказание','Люди живут и носят одни и те же имена – и разные, почти карнавальные, маски. Кто отличит героя от предателя, а шлюху – от святой? Различия в затерянном мире городка Макондо очень условны.','5-02-011850-9.','1'],
            ['Идиот','Первым произведением, вышедшим после присуждения Маркесу Нобелевской премии, стал «самый оптимистичный» роман Гарсия Маркеса «Любовь во время чумы» (1985)','5-02-013230-9.','1'],
            ['Бесы','Эта книга – о любви. О любви, настигшей человека в конце жизни, которую он прожил бездарно, растрачивая тело на безлюбый секс и не затрачивая души. ','5-02-013850-9.','1'],
            ['Дневник писателя','«Генерал в своем лабиринте» (1989) – один из лучших романов знаменитого колумбийского прозаика Габриэля Гарсии Маркеса (род. 1928)','5-02-013850-9.','1'],
            ['Подросток','«Перед вами — продолжение культовой повести Павла Санаева «Похороните меня за плинтусом». Герой «Плинтуса» вырос, ему девятнадцать лет, и все называют его Раздолбаем.','5-02-013230-1.','1'],
            ['Кавказ','Книга взорвала отечественный книжный рынок и обрела не просто культовый, но — легендарный статус! Повесть, в которой тема взросления будто переворачивается с ног на голову и обретает черты сюрреалистического юмора!','5-02-013230-1.','2'],
            ['Кобза́рь','Нулевой километр – это начало всех дорог. Он есть в каждом городе, но самый легендарный, как известно, находится в Москве. Добравшись до него, каждый получит шанс воплотить свои мечты в реальность. Костя собирается стать известным клипмейкером.','5-02-013230-1.','2'],
            ['Пятьдесят оттенков серого','«Пятьдесят оттенков серого» – первая часть трилогии Э Л Джеймс, которая сделала автора знаменитой и побила все рекорды продаж: 15 миллионов экземпляров за три месяца','5-02-013230-1.','3'],
            ['На пятьдесят оттенков темнее','««На пятьдесят оттенков темнее» — вторая книга трилогии Э Л Джеймс «Пятьдесят оттенков», которая стала бестселлером № 1 в мире, покорив читателей откровенностью','5-02-013230-1.','3'],
            ['Захар Беркут','«««Пятьдесят оттенков свободы» – третья книга трилогии Э Л Джеймс «Пятьдесят оттенков», которая стала бестселлером № 1 в мире, покорив читателей откровенностью и чувственностью. Чем закончится история Анастейши и Кристиана? Удастся ли им сохранить','5-02-013230-1.','3'],
            ['Для домашнего очага',"это роман, основанный на цикле Assassin's Creed, написанном Оливером Боуденом (Oliver Bowden) и опубликованный Penguin Books. Это печатная версия игры Assassin's Creed II; вместо того чтобы обхватить огромный промежуток времени, действия происходят только в XV веке без упоминаний современных событий",'5-02-013230-1.','4'],
            ["Украденное счастье","это роман по мотивам одноимённой игры. Роман является прямым сиквелом другой книги — Assassin's Creed: Renaissance того же писателя, Оливера Боудена. На это раз Эцио отправляется в некогда великий Рим. В городе процветает нищета, коррупция и жестокость",'5-02-013230-1.','3'],
            ["Братство","Рим, некогда могущественный, лежит в руинах. Город поглотили боль и упадок, а его жители живут в тени правящего городом семейства Борджиа. И лишь один человек может спасти людей от тирании Борджиа – Эцио Аудиторе, Глава Ассасинов.Это путешествие испытает силы Эцио.",'5-02-013230-1.','3'],
            ["Возрождение","Преданный правящими семьями Италии, молодой человек вступает в эпические поиски мести. Чтобы вырвать с корнем коррупцию и восстановить честь своей семьи, он обучится искусству ассасина. По пути, Эцио призовет мудрость таких великих умов как Леонардо Да Винчи и Николо Макиавелли – зная, что выживание его крепко-накрепко связано с навыками, которые он должен обрести.Для союзников он станет толчком к переменам",'5-02-013230-1.','4'],
            ["ТАЙНЫЙ КРЕСТОВЫЙ ПОХОД","Никколо Поло, отец Марко, наконец-то раскрывает историю, которую хранил в секрете всю свою жизнь – историю Альтаира, одного из самых необычных асассинов Братства.Альтаир отправляется на опасное задание – оно проведет его по всей Святой земле и откроет истинный смысл Кредо асассинов",'5-02-013230-1.','4'],
            ["Откровение","Когда человек одержал победу во всех битвах и победил всех своих врагов, чего еще он может желать?В поисках ответа Эцио Аудиторе должен оставить позади всю свою жизнь, чтобы в конечном итоге найти истину",'5-02-013230-1.','4'],
            ["ПУТЕШЕСТВИЕ НЕ КОНЧАЕТСЯ","Стоит в комнате набитый книгами шкаф, у него есть тайна: он может перенести человека в любое место и время. Вместе с отцом Андрей отправляется в Африку, потом, уже один, попадает то к Тому Сойеру, то в бело- казачью станицу",'5-02-013230-1.','5'],
            ["ОДИН ","Повесть о мальчике, волею случая оказавшемся на маленьком необитаемом островке в Тихом океане. В ожидании спасателей герой повести переживает ряд приключений, проявляя незаурядное мужество",'5-02-013230-1.','5'],
            ["СЛУШАЙТЕ ПЕСНЮ ПЕРЬЕВ ","Люди карабкались в товарный вагон один за одним, как звенья длинной конвейерной цепи. Очередной ставил ногу на железную подножку, цеплялся руками за бортик пола и подтягивался. Если он делал это недостаточно быстро, ему помогал ударом автоматного ствола солдат, стоявший сбоку двери",'5-02-013230-1.','5'],
            ["СЛУШАЙТЕ ПЕСНЮ ПЕРЬЕВ ","Люди карабкались в товарный вагон один за одним, как звенья длинной конвейерной цепи. Очередной ставил ногу на железную подножку, цеплялся руками за бортик пола и подтягивался. Если он делал это недостаточно быстро, ему помогал ударом автоматного ствола солдат, стоявший сбоку двери",'5-02-013230-1.','5'],
        ]);
    }

    public function down()
    {
        echo "m160719_154138_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
