<?php

use yii\db\Migration;

class m160718_161658_add_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%book}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(30)->notNull(),
			'description' => $this->string(250)->notNull(),
			'isbn' => $this->string(14)->notNull(),
			'author_id' => $this->integer(11)->notNull()
		], $tableOptions);

		$this->createTable('{{%author}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(30)->notNull(),
			'year' => $this->integer(11)->notNull(),
		], $tableOptions);

		$this->createIndex('idx_author_id', '{{%book}}', 'author_id');
        $this->addForeignKey('fk_author_book', '{{%book}}', 'author_id', '{{%author}}', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_author_book', '{{%book}}', 'author_id', '{{%author}}', 'id');
		$this->dropTable('{{%author}}');
		$this->dropTable('{{%book}}');
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
